/**
 *  Mit dieser Klasse wird das eigentliche Ergebnis ausgegeben.
 * 
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 * 
 * 
 */
class DivisorResult {
	
	// Ausgehende Zahl, bei welcher die Divisoren gezaehlt werden
		private long result;
		
	// Anzahl der Divisoren von Result
	private long countDiv;
	
	/**
	 * @param n  Zahl mit den meisten Divisoren
	 * @param c  Anzahl Divisoren
	 */
	public DivisorResult(long n, long c) {
		result = n;
		countDiv = c;
	}
	
	/**
	 * Diese Methode gibt die Zahl mit den meisten Divisoren zurueck.
	 * @return long
	 */
	public long getResult() {
		return result;
	}
	
	/**
	 * Gibt die Anzahl Divisoren der Zahl zurueck. 
	 * @return long
	 */
	public long getCountDiv() {
		return countDiv;
	}
	/*
	 * To String Methode ueberschreibt das Object
	 */
	@Override
	public String toString() {
		return "Zahl mit maximaler Anzahl Divisoren: " + result + " (" + countDiv + " Divisoren)";
	}
}