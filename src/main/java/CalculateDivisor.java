import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * Dieses Programm gibt aus einem vorgegeben Interval Long-Zahlen diejenige Zahl zurueck,
 * welche die meisten Divisoren besitzt. Wenn mehrere die gleiche Anzahl Divisoren hat, 
 * wird die kleinste der Zahlen ausgegeben.
 * 
 * Grosse Berechnungen werden in Threads ausgef�hrt, so wird das Problem aufgeteilt.
 * 
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 */

public class CalculateDivisor implements Callable<DivisorResult>{

	long von, bis;
	int threadCount;

	/**
	 * @param von: beginnend bei der unteren Intervallgrenze
	 * @param bis: zur oberen Intervallgrenze
	 * @param threadCount: Anzahl von Threads, auf welche das Problem aufgeteilt wird
	 */
	
	public CalculateDivisor(long von, long bis, int threadCount) {
		this.von = von;
		this.bis = bis;
		this.threadCount = threadCount;

	}

	
	/**
	 * Es wird ein ThreadPool mit n Threads erstellt.
	 * Mit newFixedThreadPool() kann eine IllegalArgumentException geworfen werden, wenn der threadCount kleiner
	 * gleich 0 ist. Deshalb wird der executerPool und die submit() Funktion der Future-Class 
	 * innerhalb eines try-catch-Blocks implementiert.
	 * 
	 * @return die maximale Anzahl von Divisoren und diejenige Zahl zurueck (DivisorResult)
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	
	
	DivisorResult calculate() throws InterruptedException, ExecutionException {
		
		DivisorResult result = new DivisorResult(0,0);
		
		try{
			ExecutorService executorPool = Executors.newFixedThreadPool(threadCount);
			Future<DivisorResult> future = executorPool.submit(new CalculateDivisor(von, bis, threadCount));
			
			while (!future.isDone()){
				Thread.sleep(1000);
			}
			
			result = future.get();
			executorPool.shutdown();
			
			}
		
		catch (InterruptedException e){
			Thread.currentThread().interrupt();
		}
		catch (ExecutionException e){
		}
		catch (IllegalArgumentException e) {
			System.out.println("Bitte ein threadCount von ueber 0 eingeben!");
		}
		
		return result;
	}

	
	/**
	 * Main-Methode, prueft ob die Laenge der mitgegebenen Argumente 3 ist, wenn nicht wird der Text
	 * "Usage: CalculateDivisor <intervalStart> <intervalEnd> <threadCount>" ausgegeben und das
	 * Programm abgebrochen.
	 * 
	 * Wenn 3 Argumente mitgegeben wurden, berechnet das Programm aus dem vorgegebenen Intervall
	 * die Zahl mit dem meisten Divisoren, sollte es mehrere solche Zahlen geben, wird die kleinste
	 * dieser Zahlen ausgegeben.
	 * 
	 * @author ble
	 * 
	 */
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		if (args.length != 3) {
			System.out.println("Usage: CalculateDivisor <intervalStart> <intervalEnd> <threadCount>");
			System.exit(1);
		}
		long von = Long.parseLong(args[0]);
		long bis = Long.parseLong(args[1]);
		int threads = Integer.parseInt(args[2]);

		CalculateDivisor cp = new CalculateDivisor(von, bis, threads);
		System.out.println("Ergebnis: " + cp.calculate());
	}

	/**
	 * Diese Methode berechnet fuer jede Zahl im Intervall (mitgegebene Argumente von und bis) die
	 * maximale Anzahl der Divisoren. Gibt es mehrere Zahlen mit der selben Anzahl an Divisoren,
	 * gibt die Methode die kleinste dieser Zahlen zurueck. Die Methode wird von n Threads 
	 * im ThreadPool ausgefuehrt.
	 * 
	 * @return Gibt das DivisorResult(long result, long divisorCount) zurueck.
	 */
	
	@Override
	public DivisorResult call() throws Exception {
		long divisorCount = 0;
		long result = 0;
		
		// If-Abfrage prueft ob "von" kleiner als "bis" ist.
		
		if (bis >= von) {
			for (long i = von; i <= bis; i++) {
				
				long resultDivisorCount = getDivisorCount(i);
				
				if (resultDivisorCount > divisorCount){
					divisorCount = resultDivisorCount;
					result = i;
				}
			}
			return new DivisorResult(result, divisorCount);
		}
		// Bei leeren Intervallen oder keinen Zahlen
		else {
			return new DivisorResult(0,0);
		}
	}

	/**
	 * Die Methode getDivisorCount berechnet die Anzanhl Divisoren fuer eine einzige Zahl. 
	 * 
	 * @param zahl: f�r diese Zahl werden die Anzahl Divisoren berechnet 
	 * @return Anzahl Divisoren, ansonsten 0.
	 *
	 */

	private long getDivisorCount(long zahl){
		
		if (zahl == 1){
			return 1;
		}
		else if (zahl == 2){
			return 2;
		}
		
		else if (zahl > 2){
			
			int divisorCount = 2;	//Er startet bei zwei, da verteilt durch eins und durch sich selbst nicht mitgerechnet wird
			
			
			for(int candidate = 2; candidate <= zahl / 2; candidate++ ){
				if(zahl % candidate == 0) {
					//Der divisorCount wird um eins erh�ht
					++ divisorCount;
				}
			}
		
			return divisorCount;
		}
		else{
			return 0;
		}
	}
}






