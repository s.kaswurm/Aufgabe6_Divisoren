import java.util.concurrent.ExecutionException;
import student.TestCase;

/**
 * Dieser Test pr�ft, wenn die Argumente - Zahl 10 ("von"), die Zahl 10000 ("bis") 
 * und die Anzahl der Threads (4) mitgegeben werden, ob die Zahl mit den meisten
 * Divisoren im Intervall von 10 - 10000, 7560 mit 64 Divisoren ist.
 * 
 * Hinweis: Die Unit Tests haben einen festen Timeout von 10 sekunden - achten
 * Sie daher darauf, dass Sie das Testintervall nicht zu gross gestalten.
 * 
 * @author ble
 * 
 */

public class CalculateDivisorTest extends TestCase {

	public void testCalculate() throws InterruptedException, ExecutionException {
		CalculateDivisor.main(new String[] { "10", "10000", "4" });
		assertFuzzyEquals(
				"Ergebnis: Zahl mit maximaler Anzahl Divisoren: 7560 (64 Divisoren)\n",
				systemOut().getHistory());
	}

}

